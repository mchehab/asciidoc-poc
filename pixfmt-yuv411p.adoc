=== V4L2_PIX_FMT_YUV411P ('411P')

[[V4L2_PIX_FMT_YUV411P]]

==== Name

V4L2_PIX_FMT_YUV411P ('411P') - Format with ¼ horizontal chroma resolution, also known as YUV 4:1:1.
Planar layout as opposed to <<V4L2_PIX_FMT_Y41P>>

==== Description

This format is not commonly used. This is a planar format similar to the
4:2:2 planar format except with half as many chroma. The three
components are separated into three sub-images or planes. The Y plane is
first. The Y plane has one byte per pixel. The Cb plane immediately
follows the Y plane in memory. The Cb plane is ¼ the width of the Y
plane (and of the image). Each Cb belongs to 4 pixels all on the same
row. For example, Cb~0~ belongs to Y'~00~, Y'~01~, Y'~02~ and Y'~03~.
Following the Cb plane is the Cr plane, just like the Cb plane.

If the Y plane has pad bytes after each row, then the Cr and Cb planes
have ¼ as many pad bytes after their rows. In other words, four C x rows
(including padding) is exactly as long as one Y row (including padding).

*Byte Order..*

Each cell is one byte.

[width="100%",cols=",,,,",]
|===========================================
|start + 0: |Y'~00~ |Y'~01~ |Y'~02~ |Y'~03~
|start + 4: |Y'~10~ |Y'~11~ |Y'~12~ |Y'~13~
|start + 8: |Y'~20~ |Y'~21~ |Y'~22~ |Y'~23~
|start + 12: |Y'~30~ |Y'~31~ |Y'~32~ |Y'~33~
|start + 16: |Cb~00~ 3+|
|start + 17: |Cb~10~ 3+|
|start + 18: |Cb~20~ 3+|
|start + 19: |Cb~30~ 3+|
|start + 20: |Cr~00~ 3+|
|start + 21: |Cr~10~ 3+|
|start + 22: |Cr~20~ 3+|
|start + 23: |Cr~30~ 3+|
|===========================================

*Color Sample Location..*

[cols=",,,,,,,",]
|====================
| |0 | |1 | |2 | |3
|0 |Y | |Y |C |Y | |Y
|1 |Y | |Y |C |Y | |Y
|2 |Y | |Y |C |Y | |Y
|3 |Y | |Y |C |Y | |Y
|====================

