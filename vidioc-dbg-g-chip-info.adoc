=== ioctl VIDIOC_DBG_G_CHIP_INFO

ioctl VIDIOC_DBG_G_CHIP_INFO

==== Name

VIDIOC_DBG_G_CHIP_INFO — Identify the chips on a TV card

==== Synopsis

[cols=",",]
|======================================
|`int ioctl(` |int fd,
|  |int request,
|  |struct v4l2_dbg_chip_info *argp`)`;
|======================================

==== Arguments

_`fd`_::
  File descriptor returned by link:func-open.html[`open()`].
_`request`_::
  VIDIOC_DBG_G_CHIP_INFO
_`argp`_::
  pointer

==== Description

===== Experimental

This is an link:hist-v4l2.html#experimental[experimental] interface and
may change in the future.

For driver debugging purposes this ioctl allows test applications to
query the driver about the chips present on the TV card. Regular
applications must not use it. When you found a chip specific bug, please
contact the linux-media mailing list (https://linuxtv.org/lists.php) so
it can be fixed.

Additionally the Linux kernel must be compiled with the
`CONFIG_VIDEO_ADV_DEBUG` option to enable this ioctl.

To query the driver applications must initialize the _`match.type`_ and
_`match.addr`_ or _`match.name`_ fields of a
struct link:vidioc-dbg-g-chip-info.html#v4l2-dbg-chip-info[v4l2_dbg_chip_info]
and call `VIDIOC_DBG_G_CHIP_INFO` with a pointer to this structure. On
success the driver stores information about the selected chip in the
_`name`_ and _`flags`_ fields.

When _`match.type`_ is `V4L2_CHIP_MATCH_BRIDGE`, _`match.addr`_ selects
the nth bridge `chip' on the TV card. You can enumerate all chips by
starting at zero and incrementing _`match.addr`_ by one until
`VIDIOC_DBG_G_CHIP_INFO` fails with an EINVAL error code. The number
zero always selects the bridge chip itself, e. g. the chip connected to
the PCI or USB bus. Non-zero numbers identify specific parts of the
bridge chip such as an AC97 register block.

When _`match.type`_ is `V4L2_CHIP_MATCH_SUBDEV`, _`match.addr`_ selects
the nth sub-device. This allows you to enumerate over all sub-devices.

On success, the _`name`_ field will contain a chip name and the
_`flags`_ field will contain `V4L2_CHIP_FL_READABLE` if the driver
supports reading registers from the device or `V4L2_CHIP_FL_WRITABLE` if
the driver supports writing registers to the device.

We recommended the v4l2-dbg utility over calling this ioctl directly. It
is available from the LinuxTV v4l-dvb repository; see
https://linuxtv.org/repo/ for access instructions.

.struct v4l2_dbg_match
[cols=",,,",]
|=======================================================================
|__u32 |_`type`_ |See
link:vidioc-dbg-g-chip-info.html#name-chip-match-types[Table A.6, ``Chip
Match Types''] for a list of possible types. | 

|union |(anonymous) |  | 

|  |__u32 |_`addr`_ |Match a chip by this number, interpreted according
to the _`type`_ field.

|  |char |_`name[32]`_ |Match a chip by this name, interpreted according
to the _`type`_ field. Currently unused.
|=======================================================================

.struct v4l2_dbg_chip_info
[cols=",,",]
|=======================================================================
|struct v4l2_dbg_match |_`match`_ |How to match the chip, see
link:vidioc-dbg-g-chip-info.html#name-v4l2-dbg-match[Table A.4, “struct
v4l2_dbg_match”].

|char |_`name[32]`_ |The name of the chip.

|__u32 |_`flags`_ |Set by the driver. If `V4L2_CHIP_FL_READABLE` is set,
then the driver supports reading registers from the device. If
`V4L2_CHIP_FL_WRITABLE` is set, then it supports writing registers.

|__u32 |_`reserved[8]`_ |Reserved fields, both application and driver
must set these to 0.
|=======================================================================

.Chip Match Types
[cols=",,",]
|=======================================================================
|`V4L2_CHIP_MATCH_BRIDGE` |0 |Match the nth chip on the card, zero for
the bridge chip. Does not match sub-devices.

|`V4L2_CHIP_MATCH_SUBDEV` |4 |Match the nth sub-device.
|=======================================================================

==== Return Value

On success 0 is returned, on error -1 and the `errno` variable is set
appropriately. The generic error codes are described at the
link:gen_errors.html#gen-errors[Generic Error Codes] chapter.

EINVAL::
  The _`match_type`_ is invalid or no device could be matched.

