==== ioctl FE_DISEQC_RESET_OVERLOAD -Restores the power to the antenna subsystem, if it was powered off due
to power overload.

===== Name

ioctl FE_DISEQC_RESET_OVERLOAD -Restores the power to the antenna subsystem, if it was powered off due
to power overload.

===== Synopsis

[cols=",",]
|=====================
|`int ioctl(` |int fd,
|  |int request,
|  |void *argp`)`;
|=====================

===== Arguments

`fd`::
  FE_FD
`request`::
  FE_DISEQC_RESET_OVERLOAD

===== Description

If the bus has been automatically powered off due to power overload,
this ioctl call restores the power to the bus. The call requires
read/write access to the device. This call has no effect if the device
is manually powered off. Not all DVB adapters support this ioctl.

RETURN-VALUE-DVB

