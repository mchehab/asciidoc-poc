=== V4L2_PIX_FMT_SRGGB10 ('RG10'), V4L2_PIX_FMT_SGRBG10 ('BA10'),
V4L2_PIX_FMT_SGBRG10 ('GB10'), V4L2_PIX_FMT_SBGGR10 ('BG10') -
10-bit Bayer formats expanded to 16 bits

[[V4L2_PIX_FMT_SRGGB10]]
[[V4L2_PIX_FMT_SGRBG10]]
[[V4L2_PIX_FMT_SGBRG10]]
[[V4L2_PIX_FMT_SBGGR10]]

==== Name

V4L2_PIX_FMT_SRGGB10 ('RG10'), V4L2_PIX_FMT_SGRBG10 ('BA10'),
V4L2_PIX_FMT_SGBRG10 ('GB10'), V4L2_PIX_FMT_SBGGR10 ('BG10') -
10-bit Bayer formats expanded to 16 bits

==== Description

These four pixel formats are raw sRGB / Bayer formats with 10 bits per
colour. Each colour component is stored in a 16-bit word, with 6 unused
high bits filled with zeros. Each n-pixel row contains n/2 green samples
and n/2 blue or red samples, with alternating red and blue rows. Bytes
are stored in memory in little endian order. They are conventionally
described as GRGR... BGBG..., RGRG... GBGB..., etc. Below is an example
of one of these formats

*Byte Order..*

Each cell is one byte, high 6 bits in high bytes are 0.

[width="100%",cols=",,,,,,,,,",]
|=======================================================================
|start + 0: |B~00low~ |B~00high~ |G~01low~ |G~01high~ |B~02low~
|B~02high~ |G~03low~ |G~03high~

|start + 8: |G~10low~ |G~10high~ |R~11low~ |R~11high~ |G~12low~
|G~12high~ |R~13low~ |R~13high~

|start + 16: |B~20low~ |B~20high~ |G~21low~ |G~21high~ |B~22low~
|B~22high~ |G~23low~ |G~23high~

|start + 24: |G~30low~ |G~30high~ |R~31low~ |R~31high~ |G~32low~
|G~32high~ |R~33low~ |R~33high~
|=======================================================================

