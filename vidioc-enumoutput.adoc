=== ioctl VIDIOC_ENUMOUTPUT

ioctl VIDIOC_ENUMOUTPUT

==== Name

VIDIOC_ENUMOUTPUT — Enumerate video outputs

==== Synopsis

[cols=",",]
|===============================
|`int ioctl(` |int fd,
|  |int request,
|  |struct v4l2_output *argp`)`;
|===============================

==== Arguments

_`fd`_::
  File descriptor returned by link:func-open.html[`open()`].
_`request`_::
  VIDIOC_ENUMOUTPUT
_`argp`_::
  pointer

==== Description

To query the attributes of a video outputs applications initialize the
_`index`_ field of
struct link:vidioc-enumoutput.html#v4l2-output[v4l2_output] and call the
`VIDIOC_ENUMOUTPUT` ioctl with a pointer to this structure. Drivers fill
the rest of the structure or return an EINVAL error code when the index
is out of bounds. To enumerate all outputs applications shall begin at
index zero, incrementing by one until the driver returns EINVAL.

.struct v4l2_output

[cols=",,",]
|=======================================================================
|__u32 |_`index`_ |Identifies the output, set by the application.

|__u8 |__`name`__[32] |Name of the video output, a NUL-terminated ASCII
string, for example: ``Vout''. This information is intended for the
user, preferably the connector label on the device itself.

|__u32 |_`type`_ |Type of the output, see
link:vidioc-enumoutput.html#output-type[Table A.44, ``Output Type''].

|__u32 |_`audioset`_ a|
Drivers can enumerate up to 32 video and audio outputs. This field shows
which audio outputs were selectable as the current output if this was
the currently selected video output. It is a bit mask. The LSB
corresponds to audio output 0, the MSB to output 31. Any number of bits
can be set, or none.

When the driver does not enumerate audio outputs no bits must be set.
Applications shall not interpret this as lack of audio support. Drivers
may automatically select audio outputs without enumerating them.

For details on audio outputs and how to select the current output see
link:audio.html[the section called ``Audio Inputs and Outputs''].

|__u32 |_`modulator`_ |Output devices can have zero or more RF
modulators. When the _`type`_ is `V4L2_OUTPUT_TYPE_MODULATOR` this is an
RF connector and this field identifies the modulator. It corresponds to
struct link:vidioc-g-modulator.html#v4l2-modulator[v4l2_modulator] field
__`index`__. For details on modulators see link:tuner.html[the section
called ``Tuners and Modulators''].

|link:vidioc-enumstd.html#v4l2-std-id[v4l2_std_id] |_`std`_ |Every video
output supports one or more different video standards. This field is a
set of all supported standards. For details on video standards and how
to switch see link:standard.html[the section called ``Video
Standards''].

|__u32 |_`capabilities`_ |This field provides capabilities for the
output. See link:vidioc-enumoutput.html#output-capabilities[Table A.45,
``Output capabilities''] for flags.

|__u32 |__`reserved`__[3] |Reserved for future extensions. Drivers must
set the array to zero.
|=======================================================================

.Output Type

[cols=",,",]
|=======================================================================
|`V4L2_OUTPUT_TYPE_MODULATOR` |1 |This output is an analog TV modulator.

|`V4L2_OUTPUT_TYPE_ANALOG` |2 |Analog baseband output, for example
Composite / CVBS, S-Video, RGB.

|`V4L2_OUTPUT_TYPE_ANALOGVGAOVERLAY` |3 |[?]
|=======================================================================

.Output capabilities

[cols=",,",]
|=======================================================================
|`V4L2_OUT_CAP_DV_TIMINGS` |0x00000002 |This output supports setting
video timings by using VIDIOC_S_DV_TIMINGS.

|`V4L2_OUT_CAP_STD` |0x00000004 |This output supports setting the TV
standard by using VIDIOC_S_STD.

|`V4L2_OUT_CAP_NATIVE_SIZE` |0x00000008 |This output supports setting
the native size using the `V4L2_SEL_TGT_NATIVE_SIZE` selection target,
see link:apb.html#v4l2-selections-common[the section called ``Common
selection definitions''].
|=======================================================================

==== Return Value

On success 0 is returned, on error -1 and the `errno` variable is set
appropriately. The generic error codes are described at the
link:gen_errors.html#gen-errors[Generic Error Codes] chapter.

EINVAL::
  The struct link:vidioc-enumoutput.html#v4l2-output[v4l2_output]
  _`index`_ is out of bounds.

