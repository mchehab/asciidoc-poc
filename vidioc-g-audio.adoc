=== ioctl VIDIOC_G_AUDIO, VIDIOC_S_AUDIO

ioctl VIDIOC_G_AUDIO, VIDIOC_S_AUDIO

==== Name

VIDIOC_G_AUDIO, VIDIOC_S_AUDIO — Query or select the current audio input
and its attributes

==== Synopsis

[cols=",",]
|==============================
|`int ioctl(` |int fd,
|  |int request,
|  |struct v4l2_audio *argp`)`;
|==============================


[cols=",",]
|====================================
|`int ioctl(` |int fd,
|  |int request,
|  |const struct v4l2_audio *argp`)`;
|====================================


==== Arguments

_`fd`_::
  File descriptor returned by link:func-open.html[`open()`].
_`request`_::
  VIDIOC_G_AUDIO, VIDIOC_S_AUDIO
_`argp`_::
  pointer

==== Description

To query the current audio input applications zero out the _`reserved`_
array of a struct link:vidioc-g-audio.html#v4l2-audio[v4l2_audio] and
call the `VIDIOC_G_AUDIO` ioctl with a pointer to this structure.
Drivers fill the rest of the structure or return an EINVAL error code
when the device has no audio inputs, or none which combine with the
current video input.

Audio inputs have one writable property, the audio mode. To select the
current audio input _and_ change the audio mode, applications initialize
the _`index`_ and _`mode`_ fields, and the _`reserved`_ array of a
v4l2_audio structure and call the `VIDIOC_S_AUDIO` ioctl. Drivers may
switch to a different audio mode if the request cannot be satisfied.
However, this is a write-only ioctl, it does not return the actual new
audio mode.

.struct v4l2_audio

[cols=",,",]
|=======================================================================
|__u32 |_`index`_ |Identifies the audio input, set by the driver or
application.

|__u8 |__`name`__[32] |Name of the audio input, a NUL-terminated ASCII
string, for example: ``Line In''. This information is intended for the
user, preferably the connector label on the device itself.

|__u32 |_`capability`_ |Audio capability flags, see
link:vidioc-g-audio.html#audio-capability[Table A.52, ``Audio Capability
Flags''].

|__u32 |_`mode`_ |Audio mode flags set by drivers and applications (on
`VIDIOC_S_AUDIO` ioctl), see
link:vidioc-g-audio.html#audio-mode[Table A.53, ``Audio Mode Flags''].

|__u32 |__`reserved`__[2] |Reserved for future extensions. Drivers and
applications must set the array to zero.
|=======================================================================

.Audio Capability Flags

[cols=",,",]
|=======================================================================
|`V4L2_AUDCAP_STEREO` |0x00001 |This is a stereo input. The flag is
intended to automatically disable stereo recording etc. when the signal
is always monaural. The API provides no means to detect if stereo is
_received_, unless the audio input belongs to a tuner.

|`V4L2_AUDCAP_AVL` |0x00002 |Automatic Volume Level mode is supported.
|=======================================================================

.Audio Mode Flags

[cols=",,",]
|============================================
|`V4L2_AUDMODE_AVL` |0x00001 |AVL mode is on.
|============================================

==== Return Value

On success 0 is returned, on error -1 and the `errno` variable is set
appropriately. The generic error codes are described at the
link:gen_errors.html#gen-errors[Generic Error Codes] chapter.

EINVAL::
  No audio inputs combine with the current video input, or the number of
  the selected audio input is out of bounds or it does not combine.

'''''

[cols=",,",]
|=======================================================================
|link:vidioc-expbuf.html[Prev]  |link:user-func.html[Up]
| link:vidioc-g-audioout.html[Next]

|ioctl VIDIOC_EXPBUF  |link:index.html[Home] | ioctl VIDIOC_G_AUDOUT,
VIDIOC_S_AUDOUT
|=======================================================================

