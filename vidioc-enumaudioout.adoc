=== ioctl VIDIOC_ENUMAUDOUT

ioctl VIDIOC_ENUMAUDOUT

==== Name

VIDIOC_ENUMAUDOUT — Enumerate audio outputs

==== Synopsis

[cols=",",]
|=================================
|`int ioctl(` |int fd,
|  |int request,
|  |struct v4l2_audioout *argp`)`;
|=================================

==== Arguments

_`fd`_::
  File descriptor returned by link:func-open.html[`open()`].
_`request`_::
  VIDIOC_ENUMAUDOUT
_`argp`_::
  pointer

==== Description

To query the attributes of an audio output applications initialize the
_`index`_ field and zero out the _`reserved`_ array of a
struct link:vidioc-g-audioout.html#v4l2-audioout[v4l2_audioout] and call
the `VIDIOC_G_AUDOUT` ioctl with a pointer to this structure. Drivers
fill the rest of the structure or return an EINVAL error code when the
index is out of bounds. To enumerate all audio outputs applications
shall begin at index zero, incrementing by one until the driver returns
EINVAL.

Note connectors on a TV card to loop back the received audio signal to a
sound card are not audio outputs in this sense.

See link:vidioc-g-audioout.html[ioctl VIDIOC_G_AUDOUT,
VIDIOC_S_AUDOUT(2)] for a description of
struct link:vidioc-g-audioout.html#v4l2-audioout[v4l2_audioout].

==== Return Value

On success 0 is returned, on error -1 and the `errno` variable is set
appropriately. The generic error codes are described at the
link:gen_errors.html#gen-errors[Generic Error Codes] chapter.

EINVAL::
  The number of the audio output is out of bounds.

