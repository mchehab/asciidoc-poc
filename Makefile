all: media_api.html

media_api.html:
	asciidoctor -b html media_api.adoc

clean:
	-rm media_api.xml media_api.html media_api.pdf
	-rm -rf html-dir/*

doc_html:
	a2x --no-xmllint -f chunked media_api.adoc

rsync: media_api.html
	rsync -vuza *.adoc media_api.html fedorapeople.org:public_html/media-kabi-docs-test/asciidoc_tests

media_api.xml:
	asciidoctor -b docbook45 media_api.adoc

validate: media_api.xml
	xmllint --noent --postvalid media_api.xml --noout

pdf: media_api.xml
	xmlto --skip-validation pdf media_api.xml

chunked_html: media_api.xml
	xmlto --skip-validation -o html-dir html media_api.xml

# Add dependencies

SOURCES = audio.adoc audio.h.adoc biblio.adoc ca.adoc ca.h.adoc capture.c.adoc common.adoc compat.adoc controls.adoc demux.adoc dev-capture.adoc dev-codec.adoc dev-effect.adoc dev-event.adoc dev-osd.adoc dev-output.adoc dev-overlay.adoc dev-radio.adoc dev-raw-vbi.adoc dev-rds.adoc dev-sdr.adoc dev-sliced-vbi.adoc dev-subdev.adoc dev-teletext.adoc dmx.h.adoc driver.adoc dvbapi.adoc dvbproperty.adoc examples.adoc fdl-appendix.adoc fe-diseqc-recv-slave-reply.adoc fe-diseqc-reset-overload.adoc fe-diseqc-send-burst.adoc fe-diseqc-send-master-cmd.adoc fe-enable-high-lnb-voltage.adoc fe-get-info.adoc fe-get-property.adoc fe-read-status.adoc fe-set-frontend-tune-mode.adoc fe-set-tone.adoc fe-set-voltage.adoc frontend.adoc frontend.h.adoc frontend_legacy_api.adoc func-close.adoc func-ioctl.adoc func-mmap.adoc func-munmap.adoc func-open.adoc func-poll.adoc func-read.adoc func-select.adoc func-write.adoc gen-errors.adoc intro.adoc io.adoc keytable.c.adoc libv4l.adoc lirc_device_interface.adoc media_api.adoc media-controller.adoc media-func-close.adoc media-func-ioctl.adoc media-func-open.adoc media-indices.adoc media-ioc-device-info.adoc media-ioc-enum-entities.adoc media-ioc-enum-links.adoc media-ioc-g-topology.adoc media-ioc-setup-link.adoc media-types.adoc net.adoc net.h.adoc pixfmt.adoc pixfmt-grey.adoc pixfmt-m420.adoc pixfmt-nv12.adoc pixfmt-nv12m.adoc pixfmt-nv12mt.adoc pixfmt-nv16.adoc pixfmt-nv16m.adoc pixfmt-nv24.adoc pixfmt-packed-rgb.adoc pixfmt-packed-yuv.adoc pixfmt-sbggr16.adoc pixfmt-sbggr8.adoc pixfmt-sdr-cs08.adoc pixfmt-sdr-cs14le.adoc pixfmt-sdr-cu08.adoc pixfmt-sdr-cu16le.adoc pixfmt-sdr-ru12le.adoc pixfmt-sgbrg8.adoc pixfmt-sgrbg8.adoc pixfmt-srggb10.adoc pixfmt-srggb10alaw8.adoc pixfmt-srggb10dpcm8.adoc pixfmt-srggb10p.adoc pixfmt-srggb12.adoc pixfmt-srggb8.adoc pixfmt-uv8.adoc pixfmt-uyvy.adoc pixfmt-vyuy.adoc pixfmt-y10.adoc pixfmt-y10b.adoc pixfmt-y12.adoc pixfmt-y16.adoc pixfmt-y16-be.adoc pixfmt-y41p.adoc pixfmt-yuv410.adoc pixfmt-yuv411p.adoc pixfmt-yuv420.adoc pixfmt-yuv420m.adoc pixfmt-yuv422m.adoc pixfmt-yuv422p.adoc pixfmt-yuv444m.adoc pixfmt-yuyv.adoc pixfmt-yvyu.adoc planar-apis.adoc remote_controllers.adoc selection-api.adoc selections-common.adoc subdev-formats.adoc v4l2.adoc v4l2grab.c.adoc video.adoc videodev2.h.adoc video.h.adoc vidioc-create-bufs.adoc vidioc-cropcap.adoc vidioc-dbg-g-chip-info.adoc vidioc-dbg-g-register.adoc vidioc-decoder-cmd.adoc vidioc-dqevent.adoc vidioc-dv-timings-cap.adoc vidioc-encoder-cmd.adoc vidioc-enumaudio.adoc vidioc-enumaudioout.adoc vidioc-enum-dv-timings.adoc vidioc-enum-fmt.adoc vidioc-enum-frameintervals.adoc vidioc-enum-framesizes.adoc vidioc-enum-freq-bands.adoc vidioc-enuminput.adoc vidioc-enumoutput.adoc vidioc-enumstd.adoc vidioc-expbuf.adoc vidioc-g-audio.adoc vidioc-g-audioout.adoc vidioc-g-crop.adoc vidioc-g-ctrl.adoc vidioc-g-dv-timings.adoc vidioc-g-edid.adoc vidioc-g-enc-index.adoc vidioc-g-ext-ctrls.adoc vidioc-g-fbuf.adoc vidioc-g-fmt.adoc vidioc-g-frequency.adoc vidioc-g-input.adoc vidioc-g-jpegcomp.adoc vidioc-g-modulator.adoc vidioc-g-output.adoc vidioc-g-parm.adoc vidioc-g-priority.adoc vidioc-g-selection.adoc vidioc-g-sliced-vbi-cap.adoc vidioc-g-std.adoc vidioc-g-tuner.adoc vidioc-log-status.adoc vidioc-overlay.adoc vidioc-prepare-buf.adoc vidioc-qbuf.adoc vidioc-querybuf.adoc vidioc-querycap.adoc vidioc-queryctrl.adoc vidioc-query-dv-timings.adoc vidioc-querystd.adoc vidioc-reqbufs.adoc vidioc-s-hw-freq-seek.adoc vidioc-streamon.adoc vidioc-subdev-enum-frame-interval.adoc vidioc-subdev-enum-frame-size.adoc vidioc-subdev-enum-mbus-code.adoc vidioc-subdev-g-crop.adoc vidioc-subdev-g-fmt.adoc vidioc-subdev-g-frame-interval.adoc vidioc-subdev-g-selection.adoc vidioc-subscribe-event.adoc

media_api.html: ${SOURCES}
media_api.xml: ${SOURCES}

