=== ioctl VIDIOC_DECODER_CMD, VIDIOC_TRY_DECODER_CMD

ioctl VIDIOC_DECODER_CMD, VIDIOC_TRY_DECODER_CMD

==== Name

VIDIOC_DECODER_CMD, VIDIOC_TRY_DECODER_CMD — Execute an decoder command

==== Synopsis

[cols=",",]
|====================================
|`int ioctl(` |int fd,
|  |int request,
|  |struct v4l2_decoder_cmd *argp`)`;
|====================================

==== Arguments

_`fd`_::
  File descriptor returned by link:func-open.html[`open()`].
_`request`_::
  VIDIOC_DECODER_CMD, VIDIOC_TRY_DECODER_CMD
_`argp`_::
  pointer to struct v4l2_decoder_cmd

==== Description

These ioctls control an audio/video (usually MPEG-) decoder.
`VIDIOC_DECODER_CMD` sends a command to the decoder,
`VIDIOC_TRY_DECODER_CMD` can be used to try a command without actually
executing it. To send a command applications must initialize all fields
of a
struct link:vidioc-decoder-cmd.html#v4l2-decoder-cmd[v4l2_decoder_cmd]
and call `VIDIOC_DECODER_CMD` or `VIDIOC_TRY_DECODER_CMD` with a pointer
to this structure.

The _`cmd`_ field must contain the command code. Some commands use the
_`flags`_ field for additional information.

A `write`() or link:vidioc-streamon.html[`VIDIOC_STREAMON`] call sends
an implicit START command to the decoder if it has not been started yet.

A `close`() or link:vidioc-streamon.html[`VIDIOC_STREAMOFF`] call of a
streaming file descriptor sends an implicit immediate STOP command to
the decoder, and all buffered data is discarded.

These ioctls are optional, not all drivers may support them. They were
introduced in Linux 3.3.

.struct v4l2_decoder_cmd
[cols=",,,,",]
|=======================================================================
|__u32 |_`cmd`_ |  |  |The decoder command, see
link:vidioc-decoder-cmd.html#decoder-cmds[Table A.11, ``Decoder
Commands''].

|__u32 |_`flags`_ |  |  |Flags to go with the command. If no flags are
defined for this command, drivers and applications must set this field
to zero.

|union |(anonymous) |  |  | 

|  |struct |_`start`_ |  |Structure containing additional data for the
`V4L2_DEC_CMD_START` command.

|  |  |__s32 |_`speed`_ |Playback speed and direction. The playback
speed is defined as __`speed`__/1000 of the normal speed. So 1000 is
normal playback. Negative numbers denote reverse playback, so -1000 does
reverse playback at normal speed. Speeds -1, 0 and 1 have special
meanings: speed 0 is shorthand for 1000 (normal playback). A speed of 1
steps just one frame forward, a speed of -1 steps just one frame back.

|  |  |__u32 |_`format`_ |Format restrictions. This field is set by the
driver, not the application. Possible values are
`V4L2_DEC_START_FMT_NONE` if there are no format restrictions or
`V4L2_DEC_START_FMT_GOP` if the decoder operates on full GOPs (__Group
Of Pictures__). This is usually the case for reverse playback: the
decoder needs full GOPs, which it can then play in reverse order. So to
implement reverse playback the application must feed the decoder the
last GOP in the video file, then the GOP before that, etc. etc.

|  |struct |_`stop`_ |  |Structure containing additional data for the
`V4L2_DEC_CMD_STOP` command.

|  |  |__u64 |_`pts`_ |Stop playback at this _`pts`_ or immediately if
the playback is already past that timestamp. Leave to 0 if you want to
stop after the last frame was decoded.

|  |struct |_`raw`_ |  | 

|  |  |__u32 |__`data`__[16] |Reserved for future extensions. Drivers
and applications must set the array to zero.
|=======================================================================

.Decoder Commands
[cols=",,",]
|=======================================================================
|`V4L2_DEC_CMD_START` |0 |Start the decoder. When the decoder is already
running or paused, this command will just change the playback speed.
That means that calling `V4L2_DEC_CMD_START` when the decoder was paused
will _not_ resume the decoder. You have to explicitly call
`V4L2_DEC_CMD_RESUME` for that. This command has one flag:
`V4L2_DEC_CMD_START_MUTE_AUDIO`. If set, then audio will be muted when
playing back at a non-standard speed.

|`V4L2_DEC_CMD_STOP` |1 |Stop the decoder. When the decoder is already
stopped, this command does nothing. This command has two flags: if
`V4L2_DEC_CMD_STOP_TO_BLACK` is set, then the decoder will set the
picture to black after it stopped decoding. Otherwise the last image
will repeat. mem2mem decoders will stop producing new frames altogether.
They will send a `V4L2_EVENT_EOS` event when the last frame has been
decoded and all frames are ready to be dequeued and will set the
`V4L2_BUF_FLAG_LAST` buffer flag on the last buffer of the capture queue
to indicate there will be no new buffers produced to dequeue. This
buffer may be empty, indicated by the driver setting the _`bytesused`_
field to 0. Once the `V4L2_BUF_FLAG_LAST` flag was set, the
link:vidioc-qbuf.html[VIDIOC_DQBUF] ioctl will not block anymore, but
return an EPIPE error code. If `V4L2_DEC_CMD_STOP_IMMEDIATELY` is set,
then the decoder stops immediately (ignoring the _`pts`_ value),
otherwise it will keep decoding until timestamp >= pts or until the last
of the pending data from its internal buffers was decoded.

|`V4L2_DEC_CMD_PAUSE` |2 |Pause the decoder. When the decoder has not
been started yet, the driver will return an EPERM error code. When the
decoder is already paused, this command does nothing. This command has
one flag: if `V4L2_DEC_CMD_PAUSE_TO_BLACK` is set, then set the decoder
output to black when paused.

|`V4L2_DEC_CMD_RESUME` |3 |Resume decoding after a PAUSE command. When
the decoder has not been started yet, the driver will return an EPERM
error code. When the decoder is already running, this command does
nothing. No flags are defined for this command.
|=======================================================================

==== Return Value

On success 0 is returned, on error -1 and the `errno` variable is set
appropriately. The generic error codes are described at the
link:gen_errors.html#gen-errors[Generic Error Codes] chapter.

EINVAL::
  The _`cmd`_ field is invalid.
EPERM::
  The application sent a PAUSE or RESUME command when the decoder was
  not running.

