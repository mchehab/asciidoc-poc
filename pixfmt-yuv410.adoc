=== V4L2_PIX_FMT_YVU410 ('YVU9'), V4L2_PIX_FMT_YUV410 ('YUV9')

[[V4L2_PIX_FMT_YVU410]]
[[V4L2_PIX_FMT_YUV410]]

==== Name

V4L2_PIX_FMT_YVU410 ('YVU9'), V4L2_PIX_FMT_YUV410 ('YUV9') - Planar formats 
with ¼ horizontal and vertical chroma resolution, also known as YUV 4:1:0

==== Description

These are planar formats, as opposed to a packed format. The three
components are separated into three sub-images or planes. The Y plane is
first. The Y plane has one byte per pixel. For `V4L2_PIX_FMT_YVU410`,
the Cr plane immediately follows the Y plane in memory. The Cr plane is
¼ the width and ¼ the height of the Y plane (and of the image). Each Cr
belongs to 16 pixels, a four-by-four square of the image. Following the
Cr plane is the Cb plane, just like the Cr plane. `V4L2_PIX_FMT_YUV410`
is the same, except the Cb plane comes first, then the Cr plane.

If the Y plane has pad bytes after each row, then the Cr and Cb planes
have ¼ as many pad bytes after their rows. In other words, four Cx rows
(including padding) are exactly as long as one Y row (including
padding).

*Byte Order..*

Each cell is one byte.

[width="100%",cols=",,,,",]
|===========================================
|start + 0: |Y'~00~ |Y'~01~ |Y'~02~ |Y'~03~
|start + 4: |Y'~10~ |Y'~11~ |Y'~12~ |Y'~13~
|start + 8: |Y'~20~ |Y'~21~ |Y'~22~ |Y'~23~
|start + 12: |Y'~30~ |Y'~31~ |Y'~32~ |Y'~33~
|start + 16: |Cr~00~ 3+|
|start + 17: |Cb~00~ 3+|
|===========================================

*Color Sample Location..*

[cols=",,,,,,,",]
|===================
| |0 | |1 | |2 | |3
|0 |Y | |Y | |Y | |Y
|
|1 |Y | |Y | |Y | |Y
| | | | |C | | |
|2 |Y | |Y | |Y | |Y
|
|3 |Y | |Y | |Y | |Y
|===================

