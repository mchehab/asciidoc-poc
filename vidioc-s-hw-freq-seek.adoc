=== ioctl VIDIOC_S_HW_FREQ_SEEK

ioctl VIDIOC_S_HW_FREQ_SEEK

==== Name

VIDIOC_S_HW_FREQ_SEEK — Perform a hardware frequency seek

==== Synopsis

[cols=",",]
|=====================================
|`int ioctl(` |int fd,
|  |int request,
|  |struct v4l2_hw_freq_seek *argp`)`;
|=====================================


==== Arguments

_`fd`_::
  File descriptor returned by link:func-open.html[`open()`].
_`request`_::
  VIDIOC_S_HW_FREQ_SEEK
_`argp`_::
  pointer

==== Description

Start a hardware frequency seek from the current frequency. To do this
applications initialize the __`tuner`__, __`type`__, __`seek_upward`__,
__`wrap_around`__, __`spacing`__, _`rangelow`_ and _`rangehigh`_ fields,
and zero out the _`reserved`_ array of a
struct link:vidioc-s-hw-freq-seek.html#v4l2-hw-freq-seek[v4l2_hw_freq_seek]
and call the `VIDIOC_S_HW_FREQ_SEEK` ioctl with a pointer to this
structure.

The _`rangelow`_ and _`rangehigh`_ fields can be set to a non-zero value
to tell the driver to search a specific band. If the
struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner] _`capability`_
field has the `V4L2_TUNER_CAP_HWSEEK_PROG_LIM` flag set, these values
must fall within one of the bands returned by
link:vidioc-enum-freq-bands.html[`VIDIOC_ENUM_FREQ_BANDS`]. If the
`V4L2_TUNER_CAP_HWSEEK_PROG_LIM` flag is not set, then these values must
exactly match those of one of the bands returned by
link:vidioc-enum-freq-bands.html[`VIDIOC_ENUM_FREQ_BANDS`]. If the
current frequency of the tuner does not fall within the selected band it
will be clamped to fit in the band before the seek is started.

If an error is returned, then the original frequency will be restored.

This ioctl is supported if the `V4L2_CAP_HW_FREQ_SEEK` capability is
set.

If this ioctl is called from a non-blocking filehandle, then EAGAIN
error code is returned and no seek takes place.

.struct v4l2_hw_freq_seek

[cols=",,",]
|=======================================================================
|__u32 |_`tuner`_ |The tuner index number. This is the same value as in
the struct link:vidioc-enuminput.html#v4l2-input[v4l2_input] _`tuner`_
field and the struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner]
_`index`_ field.

|__u32 |_`type`_ |The tuner type. This is the same value as in the
struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner] _`type`_ field.
See link:vidioc-g-tuner.html#v4l2-tuner-type[Table A.88, ``enum
v4l2_tuner_type'']

|__u32 |_`seek_upward`_ |If non-zero, seek upward from the current
frequency, else seek downward.

|__u32 |_`wrap_around`_ |If non-zero, wrap around when at the end of the
frequency range, else stop seeking. The
struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner] _`capability`_
field will tell you what the hardware supports.

|__u32 |_`spacing`_ |If non-zero, defines the hardware seek resolution
in Hz. The driver selects the nearest value that is supported by the
device. If spacing is zero a reasonable default value is used.

|__u32 |_`rangelow`_ |If non-zero, the lowest tunable frequency of the
band to search in units of 62.5 kHz, or if the
struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner] _`capability`_
field has the `V4L2_TUNER_CAP_LOW` flag set, in units of 62.5 Hz or if
the struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner]
_`capability`_ field has the `V4L2_TUNER_CAP_1HZ` flag set, in units of
1 Hz. If _`rangelow`_ is zero a reasonable default value is used.

|__u32 |_`rangehigh`_ |If non-zero, the highest tunable frequency of the
band to search in units of 62.5 kHz, or if the
struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner] _`capability`_
field has the `V4L2_TUNER_CAP_LOW` flag set, in units of 62.5 Hz or if
the struct link:vidioc-g-tuner.html#v4l2-tuner[v4l2_tuner]
_`capability`_ field has the `V4L2_TUNER_CAP_1HZ` flag set, in units of
1 Hz. If _`rangehigh`_ is zero a reasonable default value is used.

|__u32 |__`reserved`__[5] |Reserved for future extensions. Applications
must set the array to zero.
|=======================================================================

==== Return Value

On success 0 is returned, on error -1 and the `errno` variable is set
appropriately. The generic error codes are described at the
link:gen_errors.html#gen-errors[Generic Error Codes] chapter.

EINVAL::
  The _`tuner`_ index is out of bounds, the _`wrap_around`_ value is not
  supported or one of the values in the __`type`__, _`rangelow`_ or
  _`rangehigh`_ fields is wrong.
EAGAIN::
  Attempted to call `VIDIOC_S_HW_FREQ_SEEK` with the filehandle in
  non-blocking mode.
ENODATA::
  The hardware seek found no channels.
EBUSY::
  Another hardware seek is already in progress.

